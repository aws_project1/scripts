#!/bin/bash

set -e

export changed_images=$(git diff --name-only $CI_COMMIT_SHA $CI_COMMIT_BEFORE_SHA | grep -v '^\.gitlab-ci\.yml$' | xargs -I {} dirname {} | sort -u)
export AWS_ACCOUNT_ID=$(aws sts get-caller-identity| jq -r ".Account")
export ECR_REPOSITORY=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com

function build_images(){
    if [ -z "$changed_images"]; then
        echo "No changes detected in docker-related folders. Skipping!"
        exit 1
    else
        for image in $changed_images
        do
            echo "Bulding image: $image:$IMAGE_VERSION"
            docker build -t $ECR_REPOSITORY/$image:$IMAGE_VERSION -f $image/Dockerfile $image
            echo "image $ECR_REPOSITORY/$image:$IMAGE_VERSION builded successfully!"
        done
    fi
}

function push_images(){
    for image in $changed_images
    do
        echo "Pushing image: $ECR_REPOSITORY/$image:$IMAGE_VERSION to registry!"
        docker image ls
        docker push $ECR_REPOSITORY/$image:$IMAGE_VERSION
        echo "image $ECR_REPOSITORY/$image:$IMAGE_VERSION pushed successfully!"
    done
}