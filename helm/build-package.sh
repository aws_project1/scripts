#!/bin/bash

function setup_helm(){
    helm plugin install https://github.com/chartmuseum/helm-push
    helm plugin install https://github.com/infog/helm-replace-values-env
    kubectl create secret docker-registry microservice-docker-registry --docker-server=$CI_REGISTRY --docker-username=$GITLAB_USER --docker-password=$DOCKER_REGISTRY_TOKEN -n $NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
    export changed_helm_charts=$(git diff --name-only ${CI_COMMIT_SHA} ${CI_COMMIT_BEFORE_SHA} | grep -v '^\.gitlab-ci\.yml$' | xargs -I {} dirname {} | cut -d '/' -f 1 | sort -u)
    export SERVER_IP=$(kubectl get pods -l app=nginx -o json | jq '.items[] | .status | .podIPs[0] | .ip' | tr -d '"')

}

function template_values(){
    for chart in $changed_helm_charts
    do
        echo $chart
        eval "echo \"$(<$chart/values.yaml)\"" > $chart/values.yaml
        cat $chart/values.yaml
        eval "echo \"$(<$chart/Chart.yaml)\"" > $chart/Chart.yaml
        cat $chart/Chart.yaml
    done
}

function build_helm_package(){
    for chart in $changed_helm_charts
    do
        echo "Adding repo for $chart :"
        helm repo add $chart --username $GITLAB_USER --password $HELM_REGISTRY_TOKEN $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/helm/$CHANNEL
        chart_name=$(cat $chart/Chart.yaml | grep $chart | cut -d " " -f2)
        echo "Linting $chart helm chart!"
        cat $chart/Chart.yaml
        helm lint $chart
        echo "Packaging $chart helm chart"
        helm package $chart
        echo "Pushing helm chart package to registry!"
        helm cm-push $chart_name-*.tgz $chart
    done
}